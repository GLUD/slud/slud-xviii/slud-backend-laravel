<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('/autores', AuthorController::class);
    $router->resource('/libros', BookController::class);
    $router->resource('/editoriales', EditorialController::class);
    $router->resource('/prestamos', LoanController::class);
    $router->resource('/reembolsos', RefundController::class);
    $router->resource('/estudiantes', StudentController::class);
});
