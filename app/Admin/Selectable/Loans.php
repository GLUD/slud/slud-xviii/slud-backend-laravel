<?php

namespace App\Admin\Selectable;

use App\Models\Loan;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Grid\Selectable;

class Loans extends Selectable
{
    public $model = Loan::class;

    public function make()
    {
        $this->column('id');
        $this->column('student.name');
        $this->column('book.name');
        $this->column('created_at');

        $this->filter(function (Filter $filter) {
            $filter->like('student.name');
        });
    }
}