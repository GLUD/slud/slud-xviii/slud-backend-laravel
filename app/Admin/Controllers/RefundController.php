<?php

namespace App\Admin\Controllers;

use App\Models\Refund;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Student;
use App\Models\Book;
use App\Models\Loan;
use App\Admin\Selectable\Loans;

class RefundController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Refund';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Refund());

        $grid->column('id', __('Id'));
        $grid->column('loan.student_id', __('Estudiante'))->display(function($id){
            $student = Student::find($id);
            return $student->name.' '.$student->lastName;
        });
        $grid->column('loan.book_id', __('Libro'))->display(function($id){
            $book = Book::find($id);
            return $book->name;
        });
        $grid->column('created_at', __('Fecha'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Refund::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('loan_id', __('Loan id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Refund());

        $form->belongsTo('loan_id', Loans::class, 'Prestamos');

        return $form;
    }
}
