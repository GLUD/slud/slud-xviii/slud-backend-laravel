<?php

namespace App\Admin\Controllers;

use App\Models\Loan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Student;
use App\Models\Book;

class LoanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Loan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Loan());

        $grid->column('id', __('Id'));
        $grid->column('student.name', __('Estudiante'));
        $grid->column('book.name', __('Libro'));
        $grid->column('created_at', __('Fecha prestamo'));
        $grid->column('updated_at', __('Ultimo reporte'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Loan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('student_id', __('Student id'));
        $show->field('book_id', __('Book id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Loan());
        $student = Student::pluck('name', 'id');
        $book = Book::pluck('name', 'id');

        $form->select('student_id', __('Student id'))->options($student);
        $form->select('book_id', __('Book id'))->options($book);

        return $form;
    }
}
