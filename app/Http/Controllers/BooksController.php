<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BooksController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $editorial)
    {
        //Trae todo los libros con sus autores, dependiendo la editorial
        return Book::WhereHasEditorialWithAuthor($editorial)->get();
    }
}
