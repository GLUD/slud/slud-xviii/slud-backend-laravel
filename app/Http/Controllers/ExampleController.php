<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;
use App\Models\Editorial;

class ExampleController extends Controller
{
    public function authorWithBooks($author){
        return Author::where('name','like','%'.$author.'%')->with('books')->get();
    }

    public function editorialWithBooks($editorial){
        return Editorial::where('name','like','%'.$editorial.'%')->with('books')->get();
    }

    //Post
    public function editorialWithBooksPost(Request $request){
        return Editorial::where('name','like','%'.$request->name.'%')->with('books')->get();
    }

    public function authorWithBooksPost(Request $request){
        return Author::where('name','like','%'.$request->name.'%')->with('books')->get();    
    }   

    public function authorWithNotLoanedBooksPost(Request $request){
        return Author::where('name','like','%'.$request->name.'%')->with(['books' => function ($query) {
            $query->doesntHave('loans');
        }])->get();
    }   

    public function noLoanBooks(){
        return Book::doesntHave('loans')->get();
    }    
   
    public function editorialWithBooksAndAuthor(Request $request){
        return Editorial::where('name','like','%'.$request->name.'%')->with('books.author')->get();
    }
}

