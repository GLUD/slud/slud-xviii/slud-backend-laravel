<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    use HasFactory;
    //Relationship
    public function loan(){
        return $this->belongsTo(Loan::class);
    }
    
    //Mutations
    public function getCreatedAtAttribute($value){
        return $value ? date('Y-m-d', strtotime($value)) : $value;
    }
}
