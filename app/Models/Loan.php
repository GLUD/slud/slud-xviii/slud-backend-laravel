<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;
    //Relationship
    public function student(){
        return $this->belongsTo(Student::class);
    }

    public function book(){
        return $this->belongsTo(Book::class);
    }

    public function refunds(){
        return $this->hasMany(Refund::class);
    }

    //Mutations
    public function getCreatedAtAttribute($value){
        return $value ? date('Y-m-d', strtotime($value)) : $value;
    }

    public function getUpdatedAtAttribute($value){
        return $value ? date('Y-m-d', strtotime($value)) : $value;
    }
}
